package com.adneombgfi.peugeotproject;

/**
 * Created by WilliamYim on 5/22/17.
 */

public class DataModel {

    // Variable qui corresponds au champs qu'on veut recuperer
    private String Id;
    private String IND_Modele_Interet;
    private String Code_Plaque;
    private String Nom_Marketing;
    private String Adresse;
    private String Ville;
    private String CP;
    private String Dep;

    //Getters and setters
    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getIND_Modele_Interet() {
        return IND_Modele_Interet;
    }

    public void setIND_Modele_Interet(String IND_Modele_Interet) {
        this.IND_Modele_Interet = IND_Modele_Interet;
    }

    public String getCode_Plaque() {
        return Code_Plaque;
    }

    public void setCode_Plaque(String Code_Plaque) {
        this.Code_Plaque = Code_Plaque;
    }

    public String getNom_Marketing() {
        return Nom_Marketing;
    }

    public void setNom_Marketing(String Nom_Marketing) {
        this.Nom_Marketing = Nom_Marketing;
    }

    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String Adresse) {
        this.Adresse = Adresse;
    }

    public String getVille() {
        return Ville;
    }

    public void setVille(String Ville) {
        this.Ville = Ville;
    }

    public String getCP() {
        return CP;
    }

    public void setCP(String CP) {
        this.CP = CP;
    }

    public String getDep() {
        return Dep;
    }

    public void setDep(String Dep) {
        this.Dep = Dep;
    }
}
