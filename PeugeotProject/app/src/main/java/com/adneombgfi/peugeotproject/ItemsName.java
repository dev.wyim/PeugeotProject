package com.adneombgfi.peugeotproject;

/**
 * Created by WilliamYim on 5/12/17.
 */

public class ItemsName {

    private String itemName;

    public ItemsName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemName() {
        return this.itemName;
    }

}
