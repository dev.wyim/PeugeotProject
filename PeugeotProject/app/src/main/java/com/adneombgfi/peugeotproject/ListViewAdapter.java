package com.adneombgfi.peugeotproject;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.BaseAdapter;
import android.widget.SearchView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import java.util.Locale;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

/**
 * Created by WilliamYim on 5/12/17.
 */

public class ListViewAdapter extends BaseAdapter {

    // Declare Variables

    Context mContext;
    LayoutInflater inflater;
    private List<ItemsName> itemNamesList = null;
    private ArrayList<ItemsName> arraylist;

    public ListViewAdapter(Context context, List<ItemsName> itemList) {
        mContext = context;
        this.itemNamesList = itemList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<ItemsName>();
        this.arraylist.addAll(itemList);
    }

    public static class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return itemNamesList.size();
    }

    @Override
    public ItemsName getItem(int position) {
        return itemNamesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, final ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.list_view_items, null);
            // Locate the TextViews in listview_item.xml
            holder.name = (TextView) view.findViewById(R.id.name);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.name.setText(itemNamesList.get(position).getItemName());


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(parent.getContext(), DetailsActivity.class);
                String message = "abc";
                intent.putExtra(EXTRA_MESSAGE, message);
                Log.d("title", itemNamesList.get(position).getItemName());

                intent.putExtra("title", itemNamesList.get(position).getItemName());
                mContext.startActivity(intent);
            }
            });

        return view;
    }



    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        itemNamesList.clear();
        if (charText.length() == 0) {
            itemNamesList.addAll(arraylist);
        } else {
            for (ItemsName wp : arraylist) {
                if (wp.getItemName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    itemNamesList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }


    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }


//    @Override
//    public void registerDataSetObserver(DataSetObserver observer) {
//
//    }
//
//    @Override
//    public void unregisterDataSetObserver(DataSetObserver observer) {
//
//    }
//
//    @Override
//    public boolean hasStableIds() {
//        return false;
//    }



    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }



}
