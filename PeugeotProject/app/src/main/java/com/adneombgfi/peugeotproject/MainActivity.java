package com.adneombgfi.peugeotproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;

import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.SearchView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    ListView list;
    ListViewAdapter adapter;
    SearchView editSearch;
    ArrayList<String> itemList = new ArrayList<String>();
    ArrayList<ItemsName> arrayList = new ArrayList<ItemsName>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = (ListView) findViewById(R.id.itemListView);



        ServiceConnectionManager serviceConnectionManager = new ServiceConnectionManager();

        serviceConnectionManager.getRetrofitArray(new IServiceConnectionManager() {

            @Override
            public void onSuccess(HashMap<Integer, String[]> item) {

                for (HashMap.Entry<Integer,String[]> entry : item.entrySet()) {
                    Integer key = entry.getKey();
                    String[] value = entry.getValue();
                    itemList.add(value[0] + "    " + value[1] + "     " + value[2] + "    " + value[3] + "    " + value[4] + "    " + value[5] + "    " + value[6] + "    " + value[7]);
                }

                for (int i = 0; i < itemList.size(); i++) {
                    ItemsName itemNames = new ItemsName(itemList.get(i));
                    // Binds all strings into an array
                    arrayList.add(itemNames);
                }

//                for (int i = 0; i < arrayList.size(); i++) {
//                    System.out.println(arrayList.get(i).getItemName());
//                }

                // Pass results to ListViewAdapter Class
                adapter = new ListViewAdapter(MainActivity.this, arrayList);

                // Binds the Adapter to the ListView
                list.setAdapter(adapter);

                // Locate the EditText in listview_main.xml
                editSearch = (SearchView) findViewById(R.id.searchBar);
                editSearch.setOnQueryTextListener(MainActivity.this);
            }

            @Override
            public void onError() {
                System.out.println("Error");
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }



    @Override
    public boolean onQueryTextSubmit(String query) {

        ArrayList<String> carNames = new ArrayList<String>();

        for (int i = 0; i < arrayList.size(); i++) {
            carNames.add(arrayList.get(i).getItemName());
        }

        Intent intent = new Intent(this, ResultsActivity.class);
        intent.putExtra("carNames", carNames);
        startActivity(intent);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text = newText;
        adapter.filter(text);
        return false;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


//    public void redirectToDetailsView(View view) {
//        Intent intent = new Intent(this, DetailsActivity.class);
//        EditText editText = (EditText) findViewById();
//        String message = editText.getText().toString();
//        intent.putExtra(EXTRA_MESSAGE, message);
//        startActivity(intent);
//    }
}
