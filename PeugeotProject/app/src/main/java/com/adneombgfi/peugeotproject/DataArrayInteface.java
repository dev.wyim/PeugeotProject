package com.adneombgfi.peugeotproject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by WilliamYim on 5/22/17.
 */

public interface DataArrayInteface {

    @GET("peugeot_schema/all")
    Call<List<DataModel>> getPeugeotDetails();

}
