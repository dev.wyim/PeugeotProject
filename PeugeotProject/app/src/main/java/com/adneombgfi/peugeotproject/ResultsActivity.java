package com.adneombgfi.peugeotproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ResultsActivity extends AppCompatActivity {

    ListView resultListView;
    TextView carName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        Intent intent = getIntent();

        final ArrayList<String> carNameList = intent.getStringArrayListExtra("carNames");

        resultListView = (ListView) findViewById(R.id.resultListView);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.list_view_items, R.id.name, carNameList);

        resultListView.setAdapter(arrayAdapter);

        resultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(view.getContext(), DetailsActivity.class);
                intent.putExtra("title", carNameList.get(position));
                startActivity(intent);
            }
        });
    }
}
