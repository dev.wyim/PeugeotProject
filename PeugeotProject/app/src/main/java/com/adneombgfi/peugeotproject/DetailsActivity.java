package com.adneombgfi.peugeotproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        String title = intent.getStringExtra("title");

        String[] splited = title.split("    ");

//        for (int i = 0; i < splited.length; i++) {
//            System.out.println( i + " " + splited[i]);
//        }
//        System.out.println(title);


        TextView idTextView = (TextView) findViewById(R.id.IDTextView);
        TextView carTextView = (TextView) findViewById(R.id.carTextView);
        TextView codePlaqueTextView = (TextView) findViewById(R.id.codePlaqueTextView);
        TextView concessionTextView = (TextView) findViewById(R.id.concessionTextView);
        TextView adressTextView = (TextView) findViewById(R.id.adressTextView);
        TextView villeTextView = (TextView) findViewById(R.id.villeTextView);
        TextView cpTextView = (TextView) findViewById(R.id.cpTextView);
        TextView depTextView = (TextView) findViewById(R.id.depTextView);

        idTextView.setText("Id: " + splited[0]);
        carTextView.setText("IND_Modele_Interet: " + splited[1]);
        codePlaqueTextView.setText("Code_Plaque: " + splited[2]);
        concessionTextView.setText("Nom_Marketing: " + splited[3]);
        adressTextView.setText("Adresse: " + splited[4]);
        villeTextView.setText("Ville: " + splited[5]);
        cpTextView.setText("CP: " + splited[6]);
        depTextView.setText("Dep: " + splited[7]);


        ArrayList<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(1f, 4));
        entries.add(new BarEntry(2f, 2));
        entries.add(new BarEntry(3f, 5));
        entries.add(new BarEntry(4f, 1));
        entries.add(new BarEntry(5f, 3));
        entries.add(new BarEntry(6f, 4));
        entries.add(new BarEntry(7f, 4));
        entries.add(new BarEntry(8f, 2));
        entries.add(new BarEntry(9f, 5));
        entries.add(new BarEntry(10f, 1));
        entries.add(new BarEntry(11f, 3));

        BarChart chart = (BarChart) findViewById(R.id.chart);

        BarDataSet dataset = new BarDataSet(entries, "# of car sold");

        final ArrayList<String> labels = new ArrayList<>();
        labels.add("January");
        labels.add("February");
        labels.add("March");
        labels.add("April");
        labels.add("May");
        labels.add("June");
        labels.add("July");
        labels.add("August");
        labels.add("September");
        labels.add("November");
        labels.add("December");

        BarData data = new BarData(dataset);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return labels.get((int) value);
            }
        });


        chart.setData(data);

    }


}
