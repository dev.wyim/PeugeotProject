package com.adneombgfi.peugeotproject;

import android.provider.Settings;
import android.util.Log;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by WilliamYim on 5/22/17.
 */



public class ServiceConnectionManager {


    private String url = "http://10.0.2.2:3000";
    private HashMap<Integer, String[]> dataItems;


    public void getRetrofitArray(final IServiceConnectionManager callback) {


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        DataArrayInteface service = retrofit.create(DataArrayInteface.class);

        Call<List<DataModel>> call = service.getPeugeotDetails();
        System.out.println("HERE" + call);

        call.enqueue(new Callback<List<DataModel>>() {
            @Override
            public void onResponse(Call<List<DataModel>> call, Response<List<DataModel>> response) {
                try {

                    HashMap<Integer, String[]> items = new HashMap<>();
                    List<DataModel> PeugeotData = response.body();
                    for (int i = 0; i < PeugeotData.size(); i++) {

                        items.put(i, new String[]{ PeugeotData.get(i).getId(),
                                PeugeotData.get(i).getIND_Modele_Interet(),
                                PeugeotData.get(i).getCode_Plaque(),
                                PeugeotData.get(i).getNom_Marketing(),
                                PeugeotData.get(i).getAdresse(),
                                PeugeotData.get(i).getVille(),
                                PeugeotData.get(i).getCP(),
                                PeugeotData.get(i).getDep()
                        });

                        System.out.println(PeugeotData.get(i).getDep());
                    };
                    callback.onSuccess(items);

                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                    callback.onError();
                }
            }

            @Override
            public void onFailure(Call<List<DataModel>> call, Throwable t) {
                Log.d("onFailure", t.toString());

            }
        });
    }



}
