package com.adneombgfi.peugeotproject;

import java.util.HashMap;

/**
 * Created by WilliamYim on 5/22/17.
 */

public interface IServiceConnectionManager {



    void onSuccess(HashMap<Integer, String[]> item);
    void onError();


}
